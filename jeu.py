from donnees import *
from fonction import *

#le principe est que chaque mot enrégistrer dans données.py vienne au hasard 
# et tu devines d' abord la premire lettre et ainsi de suite 
# et tout utilisateur et sn score sera enrégistrer
#  dans le fichier scores en binaire
# le score finale est égale à (nombre de chance - nombre de fois raté)

scores = recup_scores()
utilisateur = recup_nom_utilisateur()

if utilisateur not in scores.keys():
  print("Salut CE MESSAGE S AFFICHE SEULEMENT POUR LES NOUVEAUX UTILISATEURS")

  scores[utilisateur] = 0 
continuer_partie = 'o'
 
while continuer_partie != 'n':
    print("Joueur {0}: Vous avez {1} point(s)".format(utilisateur,scores[utilisateur]))
    mot_a_trouver = choisir_mot()
    lettres_trouvees = []
    mot_trouve = recup_mot_masque(mot_a_trouver, lettres_trouvees)
    nb_chances = nb_coups

    while mot_trouve!=mot_a_trouver and nb_chances>0 and mot_trouve!=None:
      print("Mot à trouver {0} (encore {1} chances)".format(mot_trouve, nb_chances))
      lettre = recup_lettre()

      if lettre in lettres_trouvees: 
        print("Vous avez déjà choisi cette lettre.")
      elif lettre in mot_a_trouver:
        lettres_trouvees.append(lettre)
        print("Bien joué.")
      else:
        nb_chances -= 1
        print("cette lettre ne se trouve pas dans le mot")
      mot_trouve = recup_mot_masque(mot_a_trouver,lettres_trouvees)
      continue

    print("Le mot caché est: {0}".format(mot_a_trouver))

      
    scores[utilisateur] += nb_chances
    continuer_partie = input(" Souhaitez-vous continuer la partie (O/N) ?")
    continuer_partie = continuer_partie.lower()

    enregistrer_scores(scores)

print(utilisateur+" Vous finissez la partie avec {0} points.".format(scores[utilisateur]))
    